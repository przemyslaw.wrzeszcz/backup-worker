const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
const { Router } = require('express');
const HttpStatus = require('http-status-codes');
const isEmpty = require('locutus/php/var/empty');
const appConfig = require('../app-config.json');
const ResponseHelper = require('../helpers/response');

const router = Router();

router.get('/backup', (req, res, next) => {
  if (isEmpty(req.query.token) || req.query.token !== appConfig.token) {
    next(ResponseHelper.createError(HttpStatus.FORBIDDEN));
    return;
  }

  try {
    // eslint-disable-next-line no-bitwise
    fs.accessSync(path.join(__dirname, `../runtime/${appConfig.serviceSlug}_backup.loc`), fs.constants.F_OK | fs.constants.R_OK);

    next(ResponseHelper.createError(HttpStatus.CONFLICT, 'Backup already in progress'));
    return;
    // eslint-disable-next-line no-empty
  } catch (err) {}

  fs.writeFileSync(path.join(__dirname, `../runtime/${appConfig.serviceSlug}_backup.loc`), '');

  res.json({
    slug: appConfig.serviceSlug,
    message: 'Backup started',
  });
  res.end();

  exec(appConfig.backup_command, () => {
    fs.unlinkSync(path.join(__dirname, `../runtime/${appConfig.serviceSlug}_backup.loc`));
  });
});

router.get('/status', (req, res, next) => {
  if (isEmpty(req.query.token) || req.query.token !== appConfig.token) {
    next(ResponseHelper.createError(HttpStatus.FORBIDDEN));
    return;
  }

  try {
    // eslint-disable-next-line no-bitwise
    fs.accessSync(path.join(__dirname, `../runtime/${appConfig.serviceSlug}_backup.loc`), fs.constants.F_OK | fs.constants.R_OK);
    const stats = fs.statSync(path.join(__dirname, `../runtime/${appConfig.serviceSlug}_backup.loc`));

    res.json({
      slug: appConfig.serviceSlug,
      birthtime: stats.birthtime,
    });
  } catch (err) {
    res.json({
      slug: appConfig.serviceSlug,
      birthtime: null,
    });
  }
});

module.exports = router;
