const fs = require('fs');
const path = require('path');
const express = require('express');
const httpErrors = require('http-errors');
const logger = require('morgan');

const ResponseHelper = require('./helpers/response');
const indexRouter = require('./routes/index');

try {
  // eslint-disable-next-line no-bitwise
  fs.accessSync(path.join(__dirname, './runtime'), fs.constants.F_OK | fs.constants.W_OK);
} catch (err) {
  fs.mkdirSync(path.join(__dirname, './runtime'));
}

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(httpErrors(404));
});

// error handler
app.use((err, req, res, next) => {
  ResponseHelper.errorResponse(res, err);
});

module.exports = app;
