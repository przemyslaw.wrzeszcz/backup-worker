module.exports = {
  apps: [
    {
      name: 'tewu-backup-worker',
      script: './bin/start',
      watch: false,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
